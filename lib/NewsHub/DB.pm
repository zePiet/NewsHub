package NewsHub::DB;
use Poet qw($poet);
use strict;
use warnings;
use base qw(Rose::DB);

__PACKAGE__->use_private_registry;
__PACKAGE__->register_db(
				driver   => 'sqlite',
				database => $poet->data_path("newsHub.db"),
			);
    
1;
