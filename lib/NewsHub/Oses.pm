package NewsHub::Oses;
use NewsHub::DB;
use strict;
use warnings;
use base qw(Rose::DB::Object);

__PACKAGE__->meta->setup(
				table		=> 'oses',
				columns		=> [ 'id','name' ],
				pk_columns	=> 'id',
			);

__PACKAGE__->meta->make_manager_class('oses');

sub init_db { NewsHub::DB->new }

1;
