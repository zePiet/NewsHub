package NewsHub::Entry;
use NewsHub::DB;
use strict;
use warnings;
use base qw(Rose::DB::Object);

__PACKAGE__->meta->setup(
				table		=> 'entries',
				columns		=> [ 'id','os','cat','prov','title','descr','link','issued','stamp' ],
				pk_columns	=> 'id',
				auto		=> 1,
			);

__PACKAGE__->meta->make_manager_class('entries');

sub init_db { NewsHub::DB->new }

1;
