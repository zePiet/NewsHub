# NewsHub

I made NewsHub to learn mason 2 templating system and the poet framework for perl.

The application do mainly three things :

  - It fetches news feeds from various selected sources and store them in an aggregated way.
  - It opens a web server serving a site which displays the news descriptions and links. The feeds can be sorted by type and category.
  - From the aggregate feeds it generates "meta" news feeds. 

I made it to be easily modified and adapted :

  - The feeds, type and category can be set freely.
  - The site uses bootstrap from twitter so it's very easy to theme and template the design of the site. There is a css file dedicated for that. 
  - There is a ini file where most of the settings can be set, like meta tags, title, description authoring, etc...

I had in mind to make it very autonomous so once installed and configured it should need very less maintenance as almost everything is automated.

The application uses [PSGI](http://search.cpan.org/~miyagawa/PSGI-1.102/) so it can :

  - Run as a standalone web server (listening on port 5000 by default).
  - Be a part of a web server like Apache with mod_perl
  - Connected to a web server (with FastCGI, SCGI) 
  - Invoked by a web server (as in plain old CGI)
  - Proxied trought (ngnx)

To ensure security, it runs under it's own user and don't required any special privilege (For non-priviliged ports).

It comes with it's own cache system, using files by default but it can be configured to use memcache instead.

### Techs

The following technologies are used :

  - Perl 5 (Already installed on most unix systems)
  - [Mason + Poet](http://www.masonhq.com)
  - [Twitter Boostrap](http://getbootstrap.com)
  - [Sqlite 3](https://www.sqlite.org)

### Installation

See the [wiki](https://gitlab.com/zePiet/NewsHub/wikis/home) for installation guidance.
### License

The MIT License (MIT)

Copyright (c) 2015 Peter Gonnissen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
