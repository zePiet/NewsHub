$(document).ready(function()
	{
		// Submit on filter form change
		$('#os').change(function() { this.form.submit(); })

		// Intercept minimize button click
		$(".btn-minim").click(function(event){
			var target=$(event.target);
			if (target.text() == '+')
				{ target.text('-'); }
			else
				{ target.text('+') };	
    	})

	for (var i = 0; i < 3; i++)
		{ if ($("#collapse"+i).length > 0) { $("#colBut"+i).trigger("click"); }; }

});
