#!/usr/bin/perl

#####################################################################
# NewsHub : Feed reader cronjob (https://gitlab.com/zePiet/NewsHub) #
# Copyright 2015 Peter Gonnissen (peter@gonnissen.info)             #
# License MIT (http://opensource.org/licenses/MIT)                  #
#####################################################################

use XML::Feed;
use strict;
use DBI;
use Try::Tiny;

binmode STDOUT, ":utf8";

my $baseName=$1 if($0=~/(.*)\/(.+)$/);

my $expiration=time-2592000; # entries expires after 30 days

my $dbh=DBI->connect( "DBI:SQLite:dbname=$baseName/../data/newsHub.db", "", "", { RaiseError =>1 }) or die $DBI::errstr;

# Remove old records from entries
#################################

my $sth = $dbh->prepare("SELECT id,stamp FROM entries");
$sth->execute or die $DBI::errstr;

while (my $row=$sth->fetchrow_hashref())
        {
		if ($row->{stamp} <= $expiration)
			{ 
				my $sth2 = $dbh->prepare("DELETE FROM entries WHERE id = ".$row->{id});
				$sth2->execute or die $DBI::errstr;
				$sth2->finish;
			} 
	}

$sth->finish;

# Get the feeds list
####################

$sth = $dbh->prepare("SELECT os,cat,feed FROM feeds");

$sth->execute or die $DBI::errstr;

my @feeds;

while (my $row=$sth->fetchrow_hashref())
        { push @feeds,( { os => $row->{os}, cat => $row->{cat}, feed => $row->{feed} } ); }

$sth->finish;

# Read the feeds and insert the entries
#######################################

foreach my $feedEntry (@feeds)
	{
		try
			{
				my $feed = XML::Feed->parse(URI->new($feedEntry->{'feed'})) or die XML::Feed->errstr;

				for my $entry ($feed->entries)
					{
						my $os=$feedEntry->{os};
						my $issued=$entry->issued;
						my $title=$entry->title;
						my $cat=$feedEntry->{cat};
						my $descr=$entry->content->body;
						my $link=$entry->link;
						my $provider=$feed->title;
						my $stamp=time-int(rand(120));
		
						$issued='-' unless length($issued);
				
						# remove styles
						$descr=~ s/<style>.*<\/style>//gs;
		
						# remove tags
						$descr=~ s|<.+?>||g;
		
						# remove leading and trailing blanks
						$descr=~ s/^\s+|\s+$//g;
						$title=~ s/^\s+|\s+$//g;

						# replace tabs by spaces
						$descr=~ s/\G[ ]{2}/\t/g;
						$title=~ s/\G[ ]{2}/\t/g;

						# remove multiple blanks
						$descr=~ s/ +/ /;
						$title=~ s/ +/ /;

						# remove crlf
						$descr=~ s/\r|\n//g;

						# truncate word boundary 
						$descr =~ s/^(.{0,800})\b.*$/$1 .../s if (length $descr > 800);
						$title =~ s/^(.{0,80})\b.*$/$1 .../s if (length $title > 80); 
	
						$descr="No description." unless length($descr);
		
						# Check if duplicate entry
						$sth = $dbh->prepare("SELECT COUNT(id) FROM entries WHERE os = $os AND cat = $cat AND prov = \"$provider\" AND title = \"$title\"");

						$sth->execute or die $DBI::errstr."\n\n";
						my $count = $sth->fetchrow_array();
						$sth->finish;

						# If not duplicate, insert
						unless ($count)
							{
								$sth=$dbh->prepare("INSERT INTO entries (os,cat,prov,title,descr,link,issued,stamp) VALUES (?,?,?,?,?,?,?,?)");
								$sth->execute($os,$cat,$provider,$title,$descr,$link,$issued,$stamp) or die $DBI::errstr;
								$sth->finish;
							}	
   					}
			}
		catch
			{
				print "Got an error with feed : ".$feedEntry->{'feed'}."\n";
			}
	}

$dbh->disconnect();
exit 0;
