------------------
--- Table oses ---
------------------

drop table if exists oses;

create table if not exists oses (
	id integer primary key autoincrement,
	name string not null
);

insert into oses (id,name) VALUES (0,"All");

-------------------------
--- Table categories ---
-------------------------

drop table if exists categories;

create table if not exists categories (
	id integer primary key autoincrement,
	name string not null
);

------------------------
--- Table news feeds ---
------------------------

drop table if exists feeds;

create table if not exists feeds (
	id integer primary key autoincrement,
	os integer not null,
	cat integer not null,
	feed string not null
);

--------------------
--- News entries ---
--------------------

drop table if exists entries;

create table if not exists entries (
        id integer primary key autoincrement,
        os integer not null,
	cat integer not null,
	prov string not null,
        title string not null,
	descr string not null,
	link string not null,
	issued string not null,
	stamp integer not null
);
