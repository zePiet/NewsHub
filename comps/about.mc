<%class>

	############################################################
	# NewsHub : About page (https://gitlab.com/zePiet/NewsHub) #
	# Copyright 2015 Peter Gonnissen (peter@gonnissen.info)    #
	# License MIT (http://opensource.org/licenses/MIT)         #
	############################################################

	my $session = $m->session;
	my $root=$conf->get('root_dir');

</%class>
		<& topMenu.mi, active=>2 &>
% if (length $about) {
		<!-- Inserted content -->		
		<% $about %>
		<!-- /Inserted content -->
% } else {
		<div class="container">
			<h2>About</h2>
			<p>
				This is the default About page. The about page can be modified by editing : <% $page %>
			</p>
		</div>
		<& footer.mi &>
% }
<%init>
	$.title($conf->get('news.title')." : About");
	
	my $page=$root."/static/pages/about.html";
	my $about;

	if (-e $page)
		{
			local $/ = undef;
			open FILE, $page or die "Couldn't open file: $!";
			binmode FILE;
			$about = <FILE>;
  			close FILE;
		}
</%init>
