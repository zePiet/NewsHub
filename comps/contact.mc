<%class>

	##############################################################
	# NewsHub : Contact page (https://gitlab.com/zePiet/NewsHub) #
	# Copyright 2015 Peter Gonnissen (peter@gonnissen.info)      #
	# License MIT (http://opensource.org/licenses/MIT)           #
	##############################################################

	has 'submit' => (default => '');
	has 'name' => (default => '');
	has 'email' => (default => '');
	has 'message' => (default => '');
	has 'capt1' => (default => 0);
	has 'capt2' => (default => 0);
	has 'human' => (default => 0);

        my $session = $m->session;

</%class>
		<& topMenu.mi, active=>3 &>
		<div class="container">
			<h2>Contact us</h2>
			<br />
			<br />
			<!-- Contact form -->
			<form class="form-horizontal" role="form" method="post" action="/contact">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" name="name" placeholder="Your name." value="<% $fname %>">
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-8">
						<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<% $femail %>">
					</div>
				</div>
				<div class="form-group">
					<label for="message" class="col-sm-2 control-label">Message</label>
						<div class="col-sm-8">
							<textarea class="form-control" rows="4" name="message"><% $fmessage %></textarea>
						</div>
					</div>
				<div class="form-group">
					<label for="human" class="col-sm-2 control-label"><% $rand1 %> + <% $rand2 %> = ?</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
						<input type="hidden" name="capt1" value="<% $rand1 %>">
						<input type="hidden" name="capt2" value="<% $rand2 %>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-2">
						<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
					</div>
				</div>
% if (@error) {
				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-2 alert alert-danger" role="alert">	
% 	foreach (@error) {
						<% $_."<br />" %>
% 	}
					</div>
				</div>
% }
% if ($session->{mailSent}) {
* $session->{mailSent}=0;
				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-2 alert alert-success" role="alert">Email successfully sent.</div>
				</div>
% }
			</form>
		</div>
		<& footer.mi &>
<%init>

	$.title($conf->get('news.title')." : Contact");

	my @error;
	my $mailSent=0;

	my $rand1=int(rand(9))+1;
	my $rand2=int(rand(9))+1;

	my $fname='';
	my $femail='';
	my $fmessage='';
	my $fhuman='';

	# Validate the form if submitted
	################################

	if ($.submit eq "Send")
		{

			# Validate name field
			if (length $.name)
				{
					$fname=$.name;
	
					# remove html
					$fname=~ s/<([^>]|\n)*>//g;

					# trim both 
					$fname=~ s/^\s+|\s+$//g;

					# max length 20
					$fname=substr($fname,0,20);
		
					# min length 5
					push @error,"<strong>Name</strong> : Minimum five characters." if ( (length $fname) < 5 );

					# Only alphanum
					push @error,"<strong>Name</strong> : Only alphanumeric characters allowed." if ($fname =~ m/[^a-zA-Z0-9]/);
				}
			else 
				{ push @error,"<strong>Name</strong> : Required field." }

			# Validate email field
			if (length $.email)
				{
					$femail=$.email;

					# remove html
					$femail=~ s/<([^>]|\n)*>//g;

					# trim both 
					$femail=~ s/^\s+|\s+$//g;

					# max length 40
					push @error,"<strong>Email</strong> : Maximum 40 characters." if ( (length $femail) > 40 );

					# min length 15
					push @error,"<strong>Email</strong> : Minimum 10 characters." if ( (length $femail) < 10 );

					# Only validate email format
					push @error,"<strong>Email</strong> : Invalid email address." unless (Email::Valid->address($femail));
				}
			else 
				{ push @error,"<strong>Email</strong> : Required field." }

			# Validate message field	
			if (length $.message)
				{
					$fmessage=$.message;

					# remove html
					$fmessage=~ s/<([^>]|\n)*>//g;

					# trim both 
					$fmessage=~ s/^\s+|\s+$//g;

					# replace multiple blanks
					$fmessage=~ s/ +/ /;

					# max length 400
					$fmessage=substr($fmessage,0,400);

					# min length 20
					push @error,"<strong>Message</strong> : Minimum 20 characters." if ( (length $fmessage) < 20 );
				}
			else 
				{ push @error,"<strong>Message</strong> : Required field." }

			# Validate and check captchas
			if (length $.human)
				{
					$fhuman=$.human;

					# remove html
					$fhuman=~ s/<([^>]|\n)*>//g;

					# trim both 
					$fhuman=~ s/^\s+|\s+$//g;

					push @error,"<strong>Captcha</strong> : Only digits allowes." unless ($fhuman eq $fhuman+0);

					push @error,"<strong>Captcha</strong> : Error." unless ( ($.capt1+$.capt2) == $fhuman );
				} 			
			else 
				{ push @error,"<strong>Captcha</strong> : Required field." }

			# Submitted with no errors
			unless (@error)
				{

                                        my $message = Email::MIME->create(
                                                                                header_str =>   [
                                                                                                        From    => $femail,
                                                                                                        To      => $conf->get('news.contact'),
                                                                                                        Subject => "A message from ".$conf->get('news.title'),
                                                                                                ],

                                                                                attributes =>   {
                                                                                                        encoding => 'quoted-printable',
                                                                                                        charset  => 'ISO-8859-1',
                                                                                                },
                                                                                body_str =>     $fmessage,
                                                                          );

                                        if (Email::Sender::Simple->try_to_send($message))
                                                {
                                                        $session->{mailSent}=1;
                                                        $m->redirect("/contact", 302);
                                                }
                                        else
                                                { push @error,"<strong>Error</strong> : Mail could not be sent."; }

                                }
		}
</%init>

