<%class>

	#########################################################
	# NewsHub : Base.mc (https://gitlab.com/zePiet/NewsHub) #
	# Copyright 2015 Peter Gonnissen (peter@gonnissen.info) #
	# License MIT (http://opensource.org/licenses/MIT)      #
	#########################################################

	has 'title' => (default => $conf->get('news.title'));

</%class>
<%augment wrap>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<% $conf->get('news.metaDescription') %>">
		<meta name="keywords" content="<% $conf->get('news.metaKeywords') %>">
		<meta name="author" content="<% $conf->get('news.metaAuthor') %>">
% $.Defer {{
		<title><% $.title %></title>
% }}
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">	
		<link rel="stylesheet" type="text/css" href="/static/css/style.css">
		<link rel="icon" type="image/png" href="<% $conf->get('news.faviconImage') %>">
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
<% inner() %>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="/static/js/functions.js"></script>
	</body>
</html>
</%augment>
