<%class>

	##########################################################################
	# NewsHub : Meta rss feeds generator (https://gitlab.com/zePiet/NewsHub) #
	# Copyright 2015 Peter Gonnissen (peter@gonnissen.info)                  #
	# License MIT (http://opensource.org/licenses/MIT)                       #
	##########################################################################

	CLASS->no_wrap;
	has 'os' => (default => 0);
	has 'cat' => (default => 1);
	$m->res->content_type("text/xml");

</%class>
<?xml version="1.0" encoding="iso-8859-1"?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
	<channel>
		<title><% $title %></title>
		<link><% $domain %>/</link>
		<description><% $conf->get('news.metaDescription') %></description>
		<language>en-us</language>
		<webMaster><% $conf->get('news.contact') %> (<% $conf->get('news.metaAuthor') %>)</webMaster>
		<managingEditor><% $conf->get('news.contact') %> (<% $conf->get('news.metaAuthor') %>)</managingEditor>
		<docs>http://blogs.law.harvard.edu/tech/rss</docs>
		<generator>newsHub</generator>
		<ttl>120</ttl>
		<image>
			<url><% $domain.$conf->get('news.logoImage') %></url>
			<title><% $title %></title>
			<link><% $domain %>/</link>
		</image>
		<atom:link href="<% HTML::Entities::encode_entities($m->req->uri) %>" rel="self" type="application/rss+xml"/>
% foreach my $item (@items) {
		<item>
			<title><% $item->{title} %></title>
			<link><% $item->{link} %></link>
			<description><![CDATA[<% $item->{descr} %>]]></description>
			<category><% $os." ".$cat %></category>
			<guid><% $item->{link} %></guid>
% my $date=HTTP::Date::str2time($item->{pubDate});
			<pubDate><% $item->{pubDate} %></pubDate>
    		</item>
% }
	</channel>
</rss>
<%init>
	my $title=$conf->get('news.title')." :";

	# Get the domain name
	my $domain=$m->req->uri;
	$domain =~ s/^(http.?:\/\/.+)\/.+$/$1/;

	my $os="";

	if ($.os > 0)
		{
			# Get the os name
 			$os=@{NewsHub::Oses::Manager->get_oses(query => [ id=> $.os] )}[0]->name;
			$title.=" $os";
		}

	# Get the category name
	my $cat=@{NewsHub::Categories::Manager->get_categories( query => [ id=> $.cat]) }[0]->name;
	$title.=" $cat";


	# Get all the items
	###################

	my %query = 	( 
				query 	=> [ cat => $.cat, os=> $.os ], 
				sort_by => 'stamp DESC', 
				limit 	=> 20, 
				offset 	=> 0
			);

	# Handle the 'All' selection in the query
	if ($.os == 0)
		{ 
			%query = 	( 
						query 	=> [ cat => $.cat ], 
						sort_by => 'stamp DESC', 
						limit 	=> 20,
						offset 	=> 0
					);
		}

	my @items;
	my $entry;

	foreach (@{NewsHub::Entry::Manager->get_entries(%query) })
		{
			$entry->{title}=$_->title;
			$entry->{descr}=$_->descr;
			$entry->{link}=HTML::Entities::encode_entities($_->link);
			$entry->{pubDate}=$_->issued;
			$entry->{author}=$_->prov;
			$entry->{pubDate}=$_->issued;
                        $entry->{pubDate}=HTTP::Date::str2time($_->issued);
                        $entry->{pubDate}=POSIX::strftime("%a, %d %b %Y %H:%M:%S %z", localtime($entry->{pubDate})) if (defined $entry->{pubDate});

			push @items,$entry;

			$entry = {};
		}

</%init>
