<%class>

	###########################################################
	# NewsHub : Main page (https://gitlab.com/zePiet/NewsHub) #
	# Copyright 2015 Peter Gonnissen (peter@gonnissen.info)   #
	# License MIT (http://opensource.org/licenses/MIT)        #
	###########################################################

	has 'cat';
	has 'os';
	has 'ltst';
	has 'off';

	my $session = $m->session;

	my $queryLimit = $conf->get('news.entriesPerPage');
	my $itemCount = 0;

	my $title;
	my $active;

</%class>
		<& topMenu.mi, active=>0,ltst=>$session->{ltst} &>
		<div class="container">
  			<div class="row">
    				<div class="col-xs-12 col-md-9">
					<h2><% $title %></h2>
					<br>
					<!-- News entries -->
% if (@entries) {
% 	foreach my $entry (@entries) {
					<div class="panel panel-<% $entry->{level} %>">
						<div class="panel-heading">
							<h1 class="panel-title">
								<button id="colBut<% $itemCount %>" type="button" class="btn btn-<% $entry->{level} %> btn-xs btn-minim" data-toggle="collapse" data-target="#collapse<% $itemCount %>">+</button>
								<a href="<% $entry->{link} %>" target="_blank"><% $entry->{osName} %><% $entry->{title} %></a>
								<% $entry->{new} %>
							</h1>
						</div>
						<div id="collapse<% $itemCount++ %>" class="panel-body collapse"> 
							<em class="news-author-<% $entry->{level} %>">&ldquo;<% $entry->{prov} %>&rdquo; on <% $entry->{pubDate} %></em>
							<div class="alert alert-info alert-<% $entry->{level} %>-news"><% $entry->{descr} %></div>
							<a type="button" href="<% $entry->{link} %>" target="_blank" class="btn btn-<% $entry->{level} %> btn-xs">Read more...</a>
						</div>
					</div>
%	}
% } else {	
					<div class="alert alert-warning" role="alert">No news to display...</div>
% }
					<!-- /News entries -->
					<!-- News entries pagination -->
					<nav>
						<ul class="pagination ">
% my $tmpOff = $session->{queryOffset}-$queryLimit;
% $tmpOff=0 if ($tmpOff < 0);
							<li><a href="/index?off=<% $tmpOff %>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
% for (my $i = 0; $i <= 9; $i++) {
% $active="";
% $active=qq( class="active") if ( ($i*$queryLimit) == $session->{queryOffset} );
    							<li<% $active %>><a href="/index?off=<% $i*$queryLimit %>"><% $i+1 %></a></li>
% }
							<li><a href="/index?off=<% $session->{queryOffset}+$queryLimit %>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
						</ul>
					</nav>
					<!-- /News entries pagination -->
				</div>
				<div class="col-xs-6 col-md-3">
					<!-- Categories -->
					<br>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title">Categories</h1>
						</div>
						<ul class="list-group">
% foreach my $key (sort keys %{$categories}) {
							<li class="list-group-item"><a href="index?cat=<% $key %>"><% $categories->{$key} %></a></li>
% }
						</ul>
					</div>
					<!-- /Categories -->
					<!-- Filters -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title">Filters</h1>
						</div>
						<div class="panel-body">
							<form action="/index" method="POST">
								<select class="form-control" name="os" id="os">
% foreach my $key (sort keys %{$oses}) {
% my $osSelected='';
% $osSelected="selected" if ($key == $session->{os});
									<option value="<% $key %>" <% $osSelected %> ><% $oses->{$key} %></option>
% }
								</select>
							</form>
						</div>
					</div>
					<!-- /Filters -->
					<!-- Syndications -->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title">Syndications</h1>
						</div>
						<div class="panel-body">
							<a href="/rss.xml?os=<% $session->{os} %>&cat=<% $session->{cat} %>" rel="self" type="application/rss+xml"><img class="img-responsive img-rounded" src="/static/images/rss.png" alt="RSS feeds"></a>
						</div>
					</div>
					<!-- /Syndications -->
				</div>
			</div>
		</div>
		<& footer.mi &> 
<%init>	
	$itemCount = 0;

	# Set the sessions variables
	############################

	$session->{os}=0 unless (defined $session->{os});
	$session->{os}=$.os if (defined $.os);

	$session->{cat}=1 unless (defined $session->{cat});
	$session->{cat}=$.cat if (defined $.cat);

	$session->{ltst}=0 unless (defined $session->{ltst});
	$session->{ltst}=$.ltst if (defined $.ltst);

	$session->{queryOffset}=0;
	$session->{queryOffset}=$.off if (defined $.off);

	# Create the oses hashref
	#########################

	my $oses;
	foreach (@{NewsHub::Oses::Manager->get_oses})
		{
			$oses->{$_->id} = $_->name;
		}

	# Create the categories hashref
	###############################

	my $categories;
	foreach (@{NewsHub::Categories::Manager->get_categories})
		{
			$categories->{$_->id} = $_->name;
		}

	# Different levels for bootstrap panel display
	##############################################

	my $levels={ 
			0	=> 'danger',
			1	=> 'info'
	  	    };

	# Create the entries table
	##########################

	my %query = 	( 
				query 	=> [ cat => $session->{cat}, os=> $session->{os}], 
				sort_by => 'stamp DESC', 
				limit 	=> $queryLimit, 
				offset 	=> $session->{queryOffset}
			);

	# Handle the 'All' selection in the query
	if ($session->{os} == 0)
		{ 
			%query = 	( 
						query 	=> [ cat => $session->{cat} ], 
						sort_by => 'stamp DESC', 
						limit 	=> $queryLimit, 
						offset 	=> $session->{queryOffset}
					);
		}

	my @entries;
	my $entry;

	foreach (@{NewsHub::Entry::Manager->get_entries(%query) })
		{
			# Add the feed if latest news was selected and the entry is newer then 3 days OR display the feed if it's not latest news
			if ( ( ($session->{ltst}) && ($_->stamp > (time-259200)) ) || (!$session->{ltst}) )
				{
					# Create the "new" badge is timestamp is less then 3 days
					$entry->{new}='';
					$entry->{new}=qq(<span class="badge badge-success">New</span>) if ( ($_->stamp > (time-259200)) and (!$session->{ltst}) );

					# Get the publication date from the orignal feed
					$entry->{pubDate}=$_->issued if (length $_->issued);

					$entry->{cat}=$_->cat;
					$entry->{os}=$_->os;
					$entry->{title}=$_->title;
					$entry->{level}=$levels->{$_->cat};
					$entry->{descr}=$_->descr;
					$entry->{prov}=$_->prov;
					$entry->{link}=$_->link;

					$entry->{title}=$oses->{$_->os}." : ".$entry->{title} unless ($session->{os});

					push @entries,$entry;

					$entry = {};
				}
		}

	# Set the title
	###############

	$title=$categories->{$session->{cat}};
	$title="Latest ".lcfirst($title) if ($session->{ltst});
	$title=$oses->{$session->{os}}." ".lcfirst($title) if ($session->{os}>0);

	$.title($conf->get('news.title')." : News");
	$.title($conf->get('news.title')." : Latest news") if ($session->{ltst});

</%init>
